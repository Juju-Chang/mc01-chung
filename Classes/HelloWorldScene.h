#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "proj.win32\Head.h"
#include "proj.win32\BoardTile.h"
#include "proj.win32\Tail.h"

class HelloWorld : public cocos2d::Layer
{
protected:
	int row = 10, col = 10;
	BoardTile BoardMat[10][10];
	BoardTile* head;
	BoardTile* foodTile;
	bool food = false;
	bool pause = false;
	Vec2 origin;
	Size visibleSize;
	Label* pauseScreen;
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

	void move(float dt);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void addLabel();
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
