#include "HelloWorldScene.h"
#include "proj.win32\Head.h"
#include "proj.win32\BoardTile.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    

    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();

//	auto  background = Sprite::create("journey bg.png");
//	background->setPosition(0,0);
//	this->addChild(background);


    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program	
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = Label::createWithTTF("Journey", "fonts/journey.ttf", 30);
	label->setColor(ccc3(0,0,0));

    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(label, 1);

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("journey bg.png");

    // position the sprite on the center of the screen
	sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

    // add the sprite as a child to this layer
    this->addChild(sprite, 0);

	int SqrSize = 50;


	//grid
	for (int h = 0; h < row; h++)
	{
		for (int i = 0; i < col; i++)
		{
			auto rectNode = DrawNode::create();

			Vec2 crds[] =
			{	
				Vec2(SqrSize * h, SqrSize * i),
				Vec2(SqrSize * (h + 1), SqrSize * i),
				Vec2(SqrSize * (h + 1), SqrSize * (i + 1)),
				Vec2(SqrSize * h, SqrSize * (i + 1))
			};

			
			rectNode->drawPolygon(crds, 4, Color4F(1.0f, 0.3f, 0.3f, 0), 1, Color4F::BLACK);
			this->addChild(rectNode);
		}
	}
	
	float x = 0.0f;
	float y = 0.0f;
	for (int i = 0; i < row; i++)
	{
		x += 25.0f;
		Vec2 curr(x, 0);
		y = 0.0f;
		for (int j = 0; j < col; j++)
		{
			y += 25.0f;
			curr.y = y;
			BoardMat[i][j] = BoardTile::BoardTile(this, 0, BoardTile::Direction::RIGHT, curr, NULL, NULL);
		}
	}

	//Put snake on board matrix
	BoardMat[0][0] = BoardTile::BoardTile(this, 3, BoardTile::Direction::RIGHT, Vec2(25, 25), NULL, NULL);
	BoardMat[0][1] = BoardTile::BoardTile(this, 2, BoardTile::Direction::RIGHT, Vec2(75, 25), NULL, BoardMat[0][0].getBoardTile());
	BoardMat[0][2] = BoardTile::BoardTile(this, 1,BoardTile::Direction::RIGHT, Vec2(125,25), NULL, BoardMat[0][1].getBoardTile());

	BoardMat[0][0].setPred(BoardMat[0][1].getBoardTile());
	BoardMat[0][1].setPred(BoardMat[0][2].getBoardTile());
	
	this->head = BoardMat[0][2].getBoardTile();
	
	
	//this->head = Head::Head(this, Vec2(125, 125), 50, Piece::Direction::RIGHT);
	
	this->schedule(schedule_selector(HelloWorld::move), .5f);

	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(HelloWorld::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(HelloWorld::keyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this); // use if your version is below cocos2d-x 3.0alpha.1
	// use this: Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this); if you are using cocos2d-x 3.0alpha.1 and later!
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}


void HelloWorld::move(float dt)
{
	if (!food)
	{
		bool empty = false;
		int x, y;
		while (empty == false)
		{
			x = rand() % 10;
			y = rand() % 10;
			CCLOG("%f, %f", (x*50.0f) + 25, (y*50.0f) + 25);
			BoardTile* tile = head;
			bool conflict = false;
			do{
				tile = tile->getSucc();
				CCLOG("%f,%f", tile->getCoords().x, tile->getCoords().y);
				if (tile->getCoords() == Vec2(25 + (x*50.0f), 25 + (y*50.0f)))
					conflict = true;
			} while (tile->getSucc() != NULL && !conflict);
			if (!conflict)
				empty = true;
		}
		food = true;
		CCLOG("FOOD %d, %d", x, y);
		BoardMat[x][y] = BoardTile::BoardTile(this, 4, BoardTile::Direction::RIGHT, Vec2(25 + (x*50.0f), 25 + (y*50.0f)), NULL, NULL);
		foodTile = BoardMat[x][y].getBoardTile();
	}
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (BoardMat[i][j].getType() == 1)
			{
				BoardMat[i][j].move();
			}
		}
	}
	if (this->head->getCoords() == this->foodTile->getCoords())
	{
		CCLOG("EATEN");
		food = false;
		int x = (this->foodTile->getCoords().x - 25) / 50;
		int y = (this->foodTile->getCoords().y - 25) / 50;
		CCLOG("COORD %d, %d REMOVING FOOD", x, y);

		BoardMat[x][y].destroy();
		BoardMat[x][y] = BoardTile::BoardTile(this, 0, BoardTile::Direction::RIGHT, Vec2(25 + (x*50.0f), 25 + (y*50.0f)), NULL, NULL);

	}
}

void HelloWorld::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_W:
		CCLOG("UP", true);
		if (this->head->getDirection() != BoardTile::Direction::DOWN)
			this->head->setDirection(BoardTile::Direction::UP);
		break;
	case EventKeyboard::KeyCode::KEY_A:
		CCLOG("LEFT", true);
		if (this->head->getDirection() != BoardTile::Direction::RIGHT)
			this->head->setDirection(BoardTile::Direction::LEFT);
		break;
	case EventKeyboard::KeyCode::KEY_S:
		CCLOG("DOWN", true);
		if (this->head->getDirection() != BoardTile::Direction::UP)
			this->head->setDirection(BoardTile::Direction::DOWN);
		break;
	case EventKeyboard::KeyCode::KEY_D:
		CCLOG("RIGHT", true);
		if (this->head->getDirection() != BoardTile::Direction::LEFT)
			this->head->setDirection(BoardTile::Direction::RIGHT);
		break;
	case EventKeyboard::KeyCode::KEY_ESCAPE:
		this->unschedule(schedule_selector(HelloWorld::move));
		if (!pause){
			pause = true;
			this->addLabel();
		}
		else
		{
			pause = false;
			this->removeChild(pauseScreen);
			this->schedule(schedule_selector(HelloWorld::move), 0.5f);
		}
		break;
	case EventKeyboard::KeyCode::KEY_R:
		if (pause){
			auto newScene = HelloWorld::createScene();
			Director::getInstance()->replaceScene(newScene);
		}
		break;
	}


}
void HelloWorld::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{

}

void HelloWorld::addLabel()
{
	pauseScreen = Label::createWithTTF("UNPAUSE [ESC]          RESTART [R]", "fonts/journey.ttf", 30);
	pauseScreen->setColor(ccc3(0, 0, 0));

	// position the label on the center of the screen
	pauseScreen->setPosition(Vec2(origin.x + visibleSize.width /2,
		origin.y + visibleSize.height - (pauseScreen->getContentSize().height * 3)));

	// add the label as a child to this layer
	this->addChild(pauseScreen, 1);
}