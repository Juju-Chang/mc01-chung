#pragma once

#ifndef BOARDTILE_H
#define BOARDTILE_H

#include "cocos2d.h"

USING_NS_CC;

class BoardTile
{
public:
	enum Direction { UP, DOWN, LEFT, RIGHT } direction;
protected:
	Layer* board;
	int type; //0 - empty, 1 - head. 2 - body, 3 - tail, 4 - food
	bool eaten = false;
	Vec2 Coordinates;
	Direction prevDirection;
	BoardTile* ahead;
	BoardTile* behind;
	Sprite* sprite;

public:
	BoardTile();
	BoardTile(Layer* bo, int t, Direction d, Vec2 c, BoardTile* a, BoardTile* b) 
		: board(bo), type(t), direction(d), Coordinates(c), ahead(a), behind(b), eaten(false)
	{
		CCLOG("TYPE: %d", t);
		switch (t){
		case 1:
			this->sprite = Sprite::create("head_right.png");
			break;
		case 2:
			this->sprite = Sprite::create("body_hori.png");
			break;
		case 3:
			this->sprite = Sprite::create("tail_right.png");
			break;
		case 4:
			this->sprite = Sprite::create("food.png");
			break;
		default:
			this->sprite = Sprite::create();
			break;
		}
		this->sprite->setPosition(Coordinates);
		this->board->addChild(sprite);
	};
	BoardTile* create(Layer* bo, int t, Direction d, Vec2 c, BoardTile* a, BoardTile* b);
	virtual void move();
	int getType();
	void eat();
	void setPred(BoardTile* pred);
	void setSucc(BoardTile* succ);
	BoardTile* getBoardTile();
	BoardTile* getSucc();
	BoardTile* getPred();
	Vec2 getCoords();
	void setEat(bool eating);
	Direction getDirection();
	Direction getPrevDirection();
	void setDirection(Direction direction);
	void destroy();
};
#endif

