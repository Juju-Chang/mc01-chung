#include "BoardTile.h"


BoardTile::BoardTile()
{
}


void BoardTile::move()
{
	//if head
	if (type == 1)	
	{
		Texture2D *texture;
		switch (direction)
		{
		case Direction::RIGHT:
			texture = Director::getInstance()->getTextureCache()->addImage("head_right.png");
			Coordinates.x += 50;
			break;
		case Direction::LEFT:
			texture = Director::getInstance()->getTextureCache()->addImage("head_left.png");
			Coordinates.x -= 50;
			break;
		case Direction::UP:
			texture = Director::getInstance()->getTextureCache()->addImage("head_up.png");
			Coordinates.y += 50;
			break;
		case Direction::DOWN:
			texture = Director::getInstance()->getTextureCache()->addImage("head_down.png");
			Coordinates.y -= 50;
			break;
		}
		sprite->setTexture(texture);
		CCLOG("[Head]New coordinates: %f, %f", Coordinates.x, Coordinates.y);
		this->sprite->setPosition(Coordinates);
		this->behind->move();
	}
	else if (type == 2)
	{
		this->prevDirection = this->direction;
		if (eaten){
			//if behind is body
			if (this->behind->getType() != 3)
				this->behind->setEat(true);
			else
			{
				BoardTile* newTile = BoardTile::create(this->board, 2, this->direction, this->Coordinates, this, this->behind);
				this->behind->ahead = newTile;
				this->behind = newTile;
			}
		}
		this->setEat(false);
		Texture2D *texture;
		switch (direction)
		{
		case Direction::RIGHT:
			texture = Director::getInstance()->getTextureCache()->addImage("body_hori.png");
			Coordinates.x += 50;
			break;
		case Direction::LEFT:
			texture = Director::getInstance()->getTextureCache()->addImage("body_hori.png");
			Coordinates.x -= 50;
			break;
		case Direction::UP:
			texture = Director::getInstance()->getTextureCache()->addImage("body_vert.png");
			Coordinates.y += 50;
			break;
		case Direction::DOWN:
			texture = Director::getInstance()->getTextureCache()->addImage("body_vert.png");
			Coordinates.y -= 50;
			break;
		}
		sprite->setTexture(texture);
		//CCLOG("[Body]New coordinates: %f, %f", Coordinates.x, Coordinates.y);
		this->sprite->setPosition(Coordinates);
		this->direction = this->ahead->getDirection();

		this->behind->move();
	}
	else if (type == 3)
	{
		Texture2D *texture;
		switch (direction)
		{
		case Direction::RIGHT:
			texture = Director::getInstance()->getTextureCache()->addImage("tail_right.png");
			Coordinates.x += 50;
			break;
		case Direction::LEFT:
			texture = Director::getInstance()->getTextureCache()->addImage("tail_left.png");
			Coordinates.x -= 50;
			break;
		case Direction::UP:
			texture = Director::getInstance()->getTextureCache()->addImage("tail_up.png");
			Coordinates.y += 50;
			break;
		case Direction::DOWN:
			texture = Director::getInstance()->getTextureCache()->addImage("tail_down.png");
			Coordinates.y -= 50;
			break;
		}
		sprite->setTexture(texture);
		this->sprite->setPosition(Coordinates);
		this->direction = this->ahead->getPrevDirection();
		if (this->behind != NULL)
			this->behind->move();
	}
}

int BoardTile::getType()
{
	return this->type;
}

void BoardTile::eat()
{
	this->behind->setEat(true);
	this->behind->move();
}

void BoardTile::setEat(bool eating)
{
	this->eaten = eating;
}

BoardTile::Direction BoardTile::getDirection()
{
	return this->direction;
}

BoardTile* BoardTile::create(Layer* bo, int t, Direction d, Vec2 c, BoardTile* a, BoardTile* b)
{
	this->board = bo;
	this->type = t;
	this->direction = d;
	this->Coordinates = c;
	this->ahead = a;
	this->behind = b;
	switch (t){
	case 1:
		this->sprite = Sprite::create("head_spr.png");
		break;
	case 2:
		this->sprite = Sprite::create("body_hori.png");
		break;
	case 3:
		this->sprite = Sprite::create("tail_right.png");
		break;
	case 4:
		this->sprite = Sprite::create("food.png");
		break;
	}
	this->sprite->setPosition(this->Coordinates);
	this->board->addChild(this->sprite);
	return this;
}

void BoardTile::setPred(BoardTile* pred)
{
	this->ahead = pred;
}

void BoardTile::setSucc(BoardTile* succ)
{
	this->behind = succ;
}

BoardTile* BoardTile::getBoardTile()
{
	return this;
}

void BoardTile::setDirection(BoardTile::Direction direction)
{
	this->direction = direction;
}

BoardTile::Direction BoardTile::getPrevDirection()
{
	return this->prevDirection;
}

BoardTile* BoardTile::getSucc()
{
	return this->behind;
}

BoardTile* BoardTile::getPred()
{
	return this->ahead;
}

Vec2 BoardTile::getCoords()
{
	return this->Coordinates;
}

void BoardTile::destroy()
{
	this->sprite->removeFromParentAndCleanup(true);
}